package com.onlyxiahui.common.dispatcher.config;

import com.onlyxiahui.framework.action.dispatcher.config.ActionConfigurer;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionBoxRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionInterceptorRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionMethodInterceptorRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.MethodArgumentResolverRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.ResultHandlerRegistry;

/**
 * Description 
 * <br>
 * Date 2019-04-20 18:42:54<br>
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ServiceActionConfigurerAdapter implements ActionConfigurer {

	@Override
	public void addAction(ActionRegistry registry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addActionBox(ActionBoxRegistry registry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addMethodArgumentResolver(MethodArgumentResolverRegistry registry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addActionInterceptor(ActionInterceptorRegistry registry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addResultHandler(ResultHandlerRegistry registry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addActionMethodInterceptor(ActionMethodInterceptorRegistry registry) {
		// TODO Auto-generated method stub
		
	}

}
