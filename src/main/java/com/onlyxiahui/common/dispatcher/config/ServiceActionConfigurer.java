package com.onlyxiahui.common.dispatcher.config;

import com.onlyxiahui.common.dispatcher.impl.ServiceActionMethodInterceptor;
import com.onlyxiahui.common.dispatcher.impl.ServiceArgumentResolver;
import com.onlyxiahui.common.dispatcher.impl.ServiceMethodArgumentResolver;
import com.onlyxiahui.common.dispatcher.impl.ServiceResultHandler;
import com.onlyxiahui.framework.action.dispatcher.extend.ResultHandler;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionMethodInterceptorRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.MethodArgumentResolverRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.ResultHandlerRegistry;

/**
 * Date 2019-01-17 21:59:57<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class ServiceActionConfigurer extends ServiceActionConfigurerAdapter {

	@Override
	public void addMethodArgumentResolver(MethodArgumentResolverRegistry registry) {
		registry.add(new ServiceMethodArgumentResolver());
		registry.add(new ServiceArgumentResolver());
	}

	@Override
	public void addResultHandler(ResultHandlerRegistry resultHandlerRegistry) {
		// resultHandlerRegistry.add(resultHandler());
	}

	public ResultHandler resultHandler() {
		ServiceResultHandler bean = new ServiceResultHandler();
		return bean;
	}

	@Override
	public void addActionMethodInterceptor(ActionMethodInterceptorRegistry registry) {
		registry.add(new ServiceActionMethodInterceptor());
	}
}
