package com.onlyxiahui.common.dispatcher.config;

import com.onlyxiahui.common.dispatcher.impl.ServiceActionInterceptor;
import com.onlyxiahui.framework.action.dispatcher.ActionDispatcher;
import com.onlyxiahui.framework.action.dispatcher.config.ActionConfigurer;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionBox;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionBoxRegistry;
import com.onlyxiahui.framework.action.dispatcher.registry.ActionInterceptorRegistry;
import com.onlyxiahui.framework.json.validator.ValidatorService;

/**
 * Date 2019-04-20 08:13:26<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class ServiceActionDispatcher extends ActionDispatcher {
	private final ValidatorService validatorService = new ValidatorService();

	ServiceActionConfigurer handlerActionConfigurer = new ServiceActionConfigurer();
	ServiceActionInterceptor handlerActionInterceptor = new ServiceActionInterceptor(this);
//	 ServiceAuthInterceptor handlerAuthInterceptor = new ServiceAuthInterceptor();

	public ServiceActionDispatcher() {
		this.init();
	}

	public ServiceActionDispatcher(ActionBox actionBox) {
		this.init();
		this.add(actionBox);
	}

	public ServiceActionDispatcher(ActionBox actionBox, String... actionLocations) {
		this.init();
		this.add(actionBox);
		this.scan(actionLocations);
	}

	public void addActionValidator(String actionValidatorConfigPath) {
		// "classpath*:/validator/main/action/*.json"
		validatorService.loadValidatorConfig(actionValidatorConfigPath);
	}

	public void setActionValidateProperty(String validateProperty) {
		handlerActionInterceptor.setValidateProperty(validateProperty);
	}

	@Override
	public void addConfig(ActionConfigurer actionConfigurer) {
		super.addConfig(actionConfigurer);
	}

	private void init() {
		handlerActionInterceptor.setValidatorService(validatorService);
		this.addConfig(handlerActionConfigurer);
		this.addConfig(new ServiceActionConfigurerAdapter() {
			@Override
			public void addActionInterceptor(ActionInterceptorRegistry registry) {
				registry.add(handlerActionInterceptor);
				// registry.add(handlerAuthInterceptor);
			}
		});
	}

	private void add(ActionBox actionBox) {
		// "com.oim.server.general.main.business.*.action"
		this.addConfig(new ServiceActionConfigurerAdapter() {
			@Override
			public void addActionBox(ActionBoxRegistry registry) {
				registry.add(actionBox);
			}
		});
	}
}
