package com.onlyxiahui.common.dispatcher;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.common.dispatcher.constant.DataConstant;
import com.onlyxiahui.common.dispatcher.util.DispatcherJsonUtil;
import com.onlyxiahui.common.message.base.HeadImpl;
import com.onlyxiahui.common.message.base.ResultHead;
import com.onlyxiahui.common.message.node.Head;
import com.onlyxiahui.common.message.util.HeadUtil;
import com.onlyxiahui.framework.action.dispatcher.ActionDispatcher;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionMessage;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionResponse;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.extend.impl.DefaultArgumentBox;

/**
 * <br>
 * Date 2019-12-03 18:03:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ServiceMessageHandler {

	private ActionDispatcher actionDispatcher;

	public ServiceMessageHandler(ActionDispatcher actionDispatcher) {
		this.actionDispatcher = actionDispatcher;
	}

	public ActionDispatcher getActionDispatcher() {
		return actionDispatcher;
	}

	public void setActionDispatcher(ActionDispatcher actionDispatcher) {
		this.actionDispatcher = actionDispatcher;
	}

	public Object onMessage(String message, String path, ActionResponse actionResponse, ArgumentBox argumentBox) {
		return doMessage(message, path, actionResponse, argumentBox);
	}

	public Object doMessage(String message, String path, ActionResponse actionResponse, ArgumentBox argumentBox) {
		ActionMessage am = resolver(message, path, argumentBox);
		argumentBox.put(ActionResponse.class, actionResponse);
		return actionDispatcher.action(am, actionResponse, argumentBox);
	}

	public ArgumentBox getArgumentBox() {
		ArgumentBox beanBox = new DefaultArgumentBox();
		return beanBox;
	}

	public ActionMessage resolver(Object requestData, String path, ArgumentBox argumentBox) {
		String message = (requestData instanceof String) ? requestData.toString() : "";
		if (DispatcherJsonUtil.maybeJson(message)) {
			JSONObject jo = JSONObject.parseObject(message);

			argumentBox.put(JSONObject.class, jo);

			boolean hasHead = jo.containsKey(DataConstant.HEAD) && jo.get(DataConstant.HEAD) instanceof JSONObject;
			if (hasHead) {
				JSONObject headElement = jo.getJSONObject(DataConstant.HEAD);
				HeadImpl head = headElement.toJavaObject(HeadImpl.class);
				ResultHead resultHead = HeadUtil.toHead(headElement);

				argumentBox.put(Head.class, head);
				argumentBox.put(HeadImpl.class, head);
				argumentBox.put(ResultHead.class, resultHead);

				String classCode = (head.getAction() == null) ? "" : head.getAction();
				String methodCode = head.getMethod();
				if (null != methodCode && !methodCode.isEmpty()) {
					path = actionDispatcher.getActionRegistry().getPath(classCode, methodCode);
				}
			}
		}
		ActionMessage am = new ActionMessageImpl();
		am.setMessage(requestData);
		am.setAction(path == null ? "" : path);
		return am;
	}

	class ActionMessageImpl implements ActionMessage {

		Object message;
		String action;
		String method;

		@Override
		public void setMessage(Object message) {
			this.message = message;
		}

		@Override
		public Object getMessage() {
			return this.message;
		}

		@Override
		public String getAction() {
			return this.action;
		}

		@Override
		public void setAction(String action) {
			this.action = action;
		}
	}
}
