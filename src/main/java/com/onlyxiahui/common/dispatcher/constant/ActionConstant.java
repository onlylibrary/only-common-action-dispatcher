package com.onlyxiahui.common.dispatcher.constant;

import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;

/**
 * 
 * <br>
 * Date 2019-12-03 16:17:57<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ActionConstant {

	/**
	 * 请求Body缓存
	 */
	public static String REQUEST_BODY_CACHE = "requestBodyCache";

	/**
	 * 请求路径
	 */
	public static String REQUEST_PATH = "requestPath";

	/**
	 * 参数存储容器
	 */
	public static String ARGUMENT_BOX = ArgumentBox.class.getName();
}
