package com.onlyxiahui.common.dispatcher.constant;

/**
 * 
 * <br>
 * Date 2019-12-03 16:17:57<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class HeadCommonConstant {

	/**
	 * 消息的id，标识消息的唯一性
	 */
	public static String KEY = "key";
	/**
	 * 请求接口名称
	 */
	public static String SERVICE = "service";
	/**
	 * 请求动作类型
	 */
	public static String ACTION = "action";
	/**
	 * 请求方法
	 */
	public static String METHOD = "method";
	/**
	 * 请求接口版本
	 */
	public static String VERSION = "version";
	/**
	 * 响应时间
	 */
	public static String TIMESTAMP = "timestamp";
}
