package com.onlyxiahui.common.dispatcher.exception;

/**
 * 
 * <br>
 * Date 2019-09-06 14:15:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class InvokeActionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvokeActionException() {
		super();
	}

	public InvokeActionException(String s) {
		super(s);
	}

	public InvokeActionException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvokeActionException(Throwable cause) {
		super(cause);
	}
}
