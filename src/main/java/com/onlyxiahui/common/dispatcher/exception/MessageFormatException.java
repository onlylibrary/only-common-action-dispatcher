package com.onlyxiahui.common.dispatcher.exception;

/**
 * 
 * <br>
 * Date 2019-09-06 14:15:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class MessageFormatException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MessageFormatException() {
		super();
	}

	public MessageFormatException(String s) {
		super(s);
	}

	public MessageFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageFormatException(Throwable cause) {
		super(cause);
	}
}
