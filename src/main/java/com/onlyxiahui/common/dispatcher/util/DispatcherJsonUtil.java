package com.onlyxiahui.common.dispatcher.util;

import java.lang.reflect.Type;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

/**
 * Date 2019-01-13 09:23:54<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class DispatcherJsonUtil {

	/**
	 * 
	 * 
	 * JSON String to Object<br>
	 * Date 2019-07-31 08:52:35<br>
	 * 
	 * @param <T>
	 * @param json
	 * @param classType
	 * @return
	 * @since 1.0.0
	 */
	public static <T> T jsonToObject(String json, Class<T> classType) {
		T o = null;
		if (null != json && !"".equals(json)) {
			o = (T) JSONObject.parseObject(json, classType);
		}

		return (T) o;
	}

	/**
	 * 
	 * JSON String to Object<br>
	 * User u=JsonHandlerUtil.jsonToObject( json, new TypeReference<User>(){}) <br>
	 * Date 2019-07-31 08:52:59<br>
	 * 
	 * @param <T>
	 * @param json
	 * @param type
	 * @return
	 * @since 1.0.0
	 */
	public static <T> T jsonToObject(String json, TypeReference<T> type) {
		T o = null;
		if (null != json && !"".equals(json)) {
			o = (T) JSONObject.parseObject(json, type);
		}

		return (T) o;
	}

	/**
	 * 
	 * 
	 * JSON String to Object<br>
	 * Date 2019-07-31 08:54:03<br>
	 * 
	 * @param <T>
	 * @param json
	 * @param type
	 * @return
	 * @since 1.0.0
	 */
	public static <T> T jsonToObject(String json, Type type) {
		T o = null;
		if (null != json && !"".equals(json)) {
			o = JSONObject.parseObject(json, type);
		}
		return o;
	}

	/**
	 * JSONObject to Object<br>
	 * 
	 * <br>
	 * Date 2019-07-31 08:54:45<br>
	 * 
	 * @param <T>
	 * @param jo
	 * @param classType
	 * @return
	 * @since 1.0.0
	 */
	public static <T> T jsonToObject(JSONObject jo, Class<T> classType) {
		T o = null;
		if (null != jo) {
			o = (T) jo.toJavaObject(classType);
		}
		return (T) o;
	}

	/**
	 * 对象转json
	 * 
	 * @param o
	 * @return
	 */
	public static String toJson(Object o) {
		String json = "";
		if (null != o) {
			json = JSONObject.toJSONString(o);
		}
		return json;
	}

	/**
	 * 集合转json
	 * 
	 * @param list
	 * @return
	 */
	public static String toJson(List<?> list) {
		if (null == list) {
			return "";
		}
		return JSONArray.toJSONString(list);
	}

	/**
	 * 判断字符也许是JSON
	 * 
	 * <br>
	 * Date 2019-07-31 08:55:05<br>
	 * 
	 * @param string
	 * @return
	 * @since 1.0.0
	 */
	public static boolean maybeJson(String string) {
		return maybeJsonArray(string) || maybeJsonObject(string);
	}

	/**
	 * 判断字符也许是JSONArray
	 * 
	 * <br>
	 * Date 2019-07-31 08:55:05<br>
	 * 
	 * @param string
	 * @return
	 * @since 1.0.0
	 */
	public static boolean maybeJsonArray(String string) {
		string = (null == string) ? string : string.trim();
		return string != null && ("null".equals(string) || (string.startsWith("[") && string.endsWith("]")));
	}

	/**
	 * 判断字符也许是JSONObject
	 * 
	 * <br>
	 * Date 2019-07-31 08:55:05<br>
	 * 
	 * @param string
	 * @return
	 * @since 1.0.0
	 */
	public static boolean maybeJsonObject(String string) {
		string = (null == string) ? string : string.trim();
		return string != null && ("null".equals(string) || (string.startsWith("{") && string.endsWith("}")));
	}
}
