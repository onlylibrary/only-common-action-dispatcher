package com.onlyxiahui.common.dispatcher.impl;

import java.lang.reflect.Method;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.executable.ExecutableValidator;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.SmartFactoryBean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.beanvalidation.OptionalValidatorFactoryBean;

import com.onlyxiahui.framework.action.dispatcher.common.ApplyInfo;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionMethodInterceptor;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionRequest;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionResponse;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;

/**
 * 
 * 
 * <br>
 * Date 2019-12-03 18:20:29<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ServiceActionMethodInterceptor implements ActionMethodInterceptor {
	Validator validator;
	// ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
	// = vf.getValidator();
	// SpringValidatorAdapter sv = new SpringValidatorAdapter(validator);

	public ServiceActionMethodInterceptor() {
//		validator = Validation.byProvider(HibernateValidator.class).configure().failFast(true).buildValidatorFactory().getValidator();
//		MethodValidationPostProcessor postProcessor = new MethodValidationPostProcessor();
//		postProcessor.setBeforeExistingAdvisors(true);
//		postProcessor.setProxyTargetClass(true);
//		postProcessor.setValidator(validator);

//		@SuppressWarnings("resource")
//		LocalValidatorFactoryBean factoryBean = new LocalValidatorFactoryBean();
//		factoryBean.setMessageInterpolator(Validation.byDefaultProvider().configure()
//				.getDefaultMessageInterpolator());
		// optionalValidatorFactoryBean.setProviderClass(HibernateValidator.class);

		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		// messageSource.setBasename("classpath:i18n/ValidationMessages");
		OptionalValidatorFactoryBean optionalValidatorFactoryBean = new OptionalValidatorFactoryBean();
		optionalValidatorFactoryBean.setValidationMessageSource(messageSource);
		optionalValidatorFactoryBean.afterPropertiesSet();
		validator = optionalValidatorFactoryBean;
	}

	@Override
	public ApplyInfo intercept(Object object, Method method, Object[] array, MethodParameter[] parameter, ActionRequest request, ActionResponse response, ArgumentBox argumentBox) {
		// TODO Auto-generated method stub
//		vf.getValidator().validate(object, groups);
		boolean approve = true;
		Object value = null;
//		ParameterMessageInterpolator

		if (!isFactoryBeanMetadataMethod(method)) {
			Class<?>[] groups = determineValidationGroups(object, method);

			// Standard Bean Validation 1.1 API
			ExecutableValidator execVal = this.validator.forExecutables();
			Method methodToValidate = method;
			Set<ConstraintViolation<Object>> result;

			try {
				result = execVal.validateParameters(object, methodToValidate, array, groups);
			} catch (IllegalArgumentException ex) {
				// Probably a generic type mismatch between interface and impl as reported in
				// SPR-12237 / HV-1011
				// Let's try to find the bridged method on the implementation class...
				methodToValidate = BridgeMethodResolver.findBridgedMethod(ClassUtils.getMostSpecificMethod(method, object.getClass()));
				result = execVal.validateParameters(object, methodToValidate, array, groups);
			}
			if (!result.isEmpty()) {
				throw new ConstraintViolationException(result);
			}
			if (null != array) {
				int size = array.length;
				for (int i = 0; i < size; i++) {
					Validated validatedAnn = parameter[i].getParameterAnnotation(Validated.class);
					if (validatedAnn != null) {
						Class<?>[] gs = validatedAnn.value();
						result = validator.validate(array[i], gs);
						if (!result.isEmpty()) {
							throw new ConstraintViolationException(result);
						}
					}
				}
			}
		}
		ApplyInfo applyInfo = new ApplyInfo();
		applyInfo.setApprove(approve);
		applyInfo.setValue(value);
		return applyInfo;
	}

	private boolean isFactoryBeanMetadataMethod(Method method) {
		Class<?> clazz = method.getDeclaringClass();

		// Call from interface-based proxy handle, allowing for an efficient check?
		if (clazz.isInterface()) {
			return ((clazz == FactoryBean.class || clazz == SmartFactoryBean.class) &&
					!"getObject".equals(method.getName()));
		}

		// Call from CGLIB proxy handle, potentially implementing a FactoryBean method?
		Class<?> factoryBeanType = null;
		if (SmartFactoryBean.class.isAssignableFrom(clazz)) {
			factoryBeanType = SmartFactoryBean.class;
		} else if (FactoryBean.class.isAssignableFrom(clazz)) {
			factoryBeanType = FactoryBean.class;
		}
		return (factoryBeanType != null && !method.getName().equals("getObject") &&
				ClassUtils.hasMethod(factoryBeanType, method.getName(), method.getParameterTypes()));
	}

	/**
	 * Determine the validation groups to validate against for the given method
	 * invocation.
	 * <p>
	 * Default are the validation groups as specified in the {@link Validated}
	 * annotation on the containing target class of the method.
	 * 
	 * @param invocation the current MethodInvocation
	 * @return the applicable validation groups as a Class array
	 */
	protected Class<?>[] determineValidationGroups(Object bean, Method method) {
		Validated validatedAnn = AnnotationUtils.findAnnotation(method, Validated.class);
		if (validatedAnn == null) {
			validatedAnn = AnnotationUtils.findAnnotation(bean.getClass(), Validated.class);
		}
		return (validatedAnn != null ? validatedAnn.value() : new Class<?>[0]);
	}
}
