package com.onlyxiahui.common.dispatcher.impl;

import com.onlyxiahui.common.dispatcher.handler.ResultDataHandler;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionRequest;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionResponse;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.extend.ResultHandler;

/**
 * Date 2019-01-12 12:05:12<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class ServiceResultHandler implements ResultHandler {

	ResultDataHandler rh = new ResultDataHandler();

	@Override
	public Object handle(Object result, ActionRequest request, ActionResponse response, ArgumentBox argumentBox) {
		Object requestData = request.getData();
		Object value = rh.result(result, requestData, argumentBox);
		return value;
	}
}
