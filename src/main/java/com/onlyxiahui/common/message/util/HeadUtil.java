package com.onlyxiahui.common.message.util;

import com.alibaba.fastjson.JSONObject;
import com.onlyxiahui.common.dispatcher.constant.HeadCommonConstant;
import com.onlyxiahui.common.message.base.ResultHead;

/**
 * 
 * <br>
 * Date 2019-12-03 17:18:24<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class HeadUtil {

	public static ResultHead toHead(JSONObject jo) {
		ResultHead head = new ResultHead();
		head.setTimestamp(System.currentTimeMillis());
		if (null != jo) {
			put(head, HeadCommonConstant.KEY, jo.get(HeadCommonConstant.KEY));
			put(head, HeadCommonConstant.SERVICE, jo.get(HeadCommonConstant.SERVICE));
			put(head, HeadCommonConstant.ACTION, jo.get(HeadCommonConstant.ACTION));
			put(head, HeadCommonConstant.METHOD, jo.get(HeadCommonConstant.METHOD));
			put(head, HeadCommonConstant.VERSION, jo.get(HeadCommonConstant.VERSION));
		}
		return head;
	}

	public static void put(ResultHead head, String key, Object value) {
		if (null != head && null != key && null != value) {
			head.put(key, value);
		}
	}
}
