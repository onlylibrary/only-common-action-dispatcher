package com.onlyxiahui.common.message.base;

/**
 * 
 * 
 * <br>
 * Date 2019-12-03 16:17:07<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class DispatcherErrorCode {

	public static final String CODE_FAIL = "0";
	public static final String CODE_SUCCESS = "1";

	public static final String MESSAGE_FORMAT_ERROR = "001";
	public static final String ACTION_ONT_FOUND = "002";
}
